package restarts;

import java.io.Serializable;
import java.util.List;
import restarts.Problem;
import restarts.Solution;

public abstract class Algorithm implements Serializable {
    
    public Algorithm(Problem p, double totalTime, Object seed) {
        this.p = p;
        this.totalTime = totalTime;
        this.seed = seed;
    };
    
    public Problem p = null;
    public double totalTime = 0;
    public Object seed = null;
    
    public abstract Solution execute();
    
    
    
    public static void printListOfStrings(List<String> list) {
        String result = "";
        for (String s:list)
            result+=s+" ";
        System.out.println(result);
    }
}
