package restarts.problems;

import java.io.Serializable;
import java.util.Arrays;
import restarts.Problem;
import restarts.Solution;

public class TTP extends Problem implements Serializable {
    
    public String subfolder = "ttpInstances";
    public String filename = null;
    
    public TTP(String ttpInstance) {
        super(Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, true); // no best value known, no worst value known, false==minimisation problem
        this.filename = ttpInstance;
    }
    
    
    
    // not needed, do not use this
    public double evaluate(Solution solution) {
        return Double.NEGATIVE_INFINITY;
    }
    // not needed, do not use this
    public Solution initialSolution() {
        return null;
    }
}
