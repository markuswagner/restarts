package restarts.problems;

import java.io.Serializable;
import java.util.Arrays;
import restarts.Problem;
import restarts.Solution;

public class MAXSAT extends Problem implements Serializable {
    
    public String subfolder = "maxsatInstances";
    public String maxsatInstance = null;
    
    public MAXSAT(String maxsatInstance) {
        super(0, Double.POSITIVE_INFINITY, false); // no best value known, no worst value known, false==minimisation problem
        this.maxsatInstance = maxsatInstance;
    }
    
    
    
    // not needed, do not use this
    public double evaluate(Solution solution) {
        return Double.POSITIVE_INFINITY;
    }
    // not needed, do not use this
    public Solution initialSolution() {
        return null;
    }
}
