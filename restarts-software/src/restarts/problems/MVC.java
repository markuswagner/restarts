package restarts.problems;

import java.io.Serializable;
import java.util.Arrays;
import restarts.Problem;
import restarts.Solution;

public class MVC extends Problem implements Serializable {
    
    public String subfolder = "vcInstances";
    public String vcInstance = null;
    
    public MVC(String tspInstance) {
        super(0, Double.POSITIVE_INFINITY, false); // no best value known, no worst value known, false==minimisation problem
        this.vcInstance = tspInstance;
    }
    
    
    
    // not needed, do not use this
    public double evaluate(Solution solution) {
        return Double.POSITIVE_INFINITY;
    }
    // not needed, do not use this
    public Solution initialSolution() {
        return null;
    }
}
