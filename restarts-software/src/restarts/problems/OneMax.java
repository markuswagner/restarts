package restarts.problems;

import java.io.Serializable;
import java.util.Arrays;
import restarts.Problem;
import restarts.Solution;

public class OneMax extends Problem implements Serializable {
    
    int n = 0;
    
//    OneMax() {
//        this.isMaximisation = true;
//    }
    
    OneMax(int n) {
        super(n,0,true);
        this.n = n;        
//        System.out.println("initialised with n "+n + " optimumValue "+optimumValue);
    }
    
    public Solution initialSolution() {
        Solution result = new Solution(n);
        for (int i=0; i<n; i++) {
            if (Math.random() < 0.5) {
                result.values[i] = 1;
            }
        }
//        System.out.println("initial solution with size "+result.length+ " optimumValue "+optimumValue);
        return result;
    }
    
    public double evaluate(Solution solution) {
        boolean debugPrint = false;
        solution.fitness = 0;
        double result = 0;
        for (int i=0; i<solution.values.length; i++) {
            solution.fitness += solution.values[i];
        }
        if (debugPrint) System.out.println(" evaluate: result="+result+" "+Arrays.toString(solution.values));
        return solution.fitness;
    }

    
}
