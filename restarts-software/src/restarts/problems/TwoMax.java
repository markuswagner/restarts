package restarts.problems;

import java.io.Serializable;
import java.util.Arrays;
import restarts.Problem;
import restarts.Solution;

public class TwoMax extends Problem implements Serializable {
    
    int n = 0;
    
    public TwoMax(int n) {
        super(Double.POSITIVE_INFINITY, 0, true);
        this.n = n;
//        this.optimumValue = 2*n;
    }
    
    public double evaluate(Solution solution) {
        boolean debugPrint = false;
        solution.fitness = 0;
        double result = 0;
        for (int i=0; i<solution.values.length; i++) {
            solution.fitness += solution.values[i];
        }
        if (debugPrint) System.out.println(" evaluate: result="+result+" "+Arrays.toString(solution.values));
        
        /* here comes the interesting part:
        if the number of 1s so far is <n/2, then the fitness is n/2 minus the number of 1s
        else double the value (as each 1 is worth 2)
        */
//        System.out.print(solution.fitness+" -> ");
        if (solution.fitness < this.n/2d) {
            solution.fitness = this.n/2d - solution.fitness;
        } else {
            solution.fitness = Math.pow(solution.fitness - this.n/2d,2) / Math.sqrt(this.n);
        }
//        } else if (solution.fitness < 3d*this.n/4d) {
////            System.out.print("XXX");
//            solution.fitness = (solution.fitness-this.n/2d)*2d;
//        } else {
//            solution.fitness = (solution.fitness-3d*this.n/4d) * 4 + this.n/2d;
//        }
//        System.out.println(solution.fitness);
        
        return solution.fitness;
    }

    // like OneMax: random bitstring
    public Solution initialSolution() {
        Solution result = new Solution(n);
        for (int i=0; i<n; i++) {
            if (Math.random() < 0.5) {
                result.values[i] = 1;
            }
        }
        return result;
    }
}
