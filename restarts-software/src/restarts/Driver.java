package restarts;

import com.sun.javafx.scene.shape.PathUtils;
import restarts.problems.MAXSAT;
import restarts.problems.TSP;
import restarts.problems.TwoMax;
import restarts.problems.MVC;
import restarts.algorithms.LKH;
import restarts.algorithms.RLS;
import restarts.algorithms.NUMVC;
import java.util.Arrays;
import java.util.Comparator;
import restarts.algorithms.ACOTSPJavaTTP;
import restarts.algorithms.CCLS2015;
import restarts.problems.TTP;
import restarts.utils.DeepCopy;

public class Driver {

    
    
    public static void generateArrayForKLStudy() {
        String base = "/home/mwagner/scratch/jdk1.8.0_05/bin/java -classpath build/classes/ restarts.Driver";
        
        /*assumptions: 
        16% of total time spent for all runs in the first phase
        40% of total time spent for all runs in the first phase + second phase (--> 24% total time spent on runs in the second phase)
        */
        
//        String[] targetInstances = tspInstances;
//        double[] targetTimes = tspInits;
//        String[] targetInstances = mvcInstances;
//        double[] targetTimes = mvcInits;
        String[] targetInstances = ttpInstances;
        double[] targetTimes = ttpInits;
//        String[] targetInstances = maxsatInstances;
//        double[] targetTimes = maxsatInits;
        
        for (int j=0; j<targetInstances.length; j++) {
            String instance = targetInstances[j];
            double tinit = targetTimes[j];
            
            // if tinit is too long, then do just a single repetition of the overall setup-
            int reps = 100; 
            if (tinit > 29) reps = 1;
            
            int tableDimensions = 30;
            
            // false: do double-logarithmic scaling with tableDimensions^2 combos in total, true: do given steps (1,4,10,...)
            boolean doCrossDomainStudy = !false; 
//            int[] tfactors = new int[]{100,400,1000};
            int[] tfactors = new int[]{1000};
            
            for (int tf=0; tf<tfactors.length; tf++) {
                int tfactor = tfactors[tf];
            
                double firstPhasePercentageOfTotal = 0.16;
                double secondPhasePercentageOfTotal = 0.24;
                double totalTimeBudget = tfactor*tinit;
                double totalTimeInFirstPhase = firstPhasePercentageOfTotal*totalTimeBudget;
                double totalTimeInSecondPhase = secondPhasePercentageOfTotal*totalTimeBudget;
                int maxk = (int)(firstPhasePercentageOfTotal*tfactor);
                int targetNumberRightCorner = maxk;
                double xAxisFactor = Math.pow ( targetNumberRightCorner, 1d/(tableDimensions-1)); // -1 since I know the very first value
                //        System.out.println("xAxisFactor="+xAxisFactor);

                // restarts array
                int[] firstPhaseRestarts = new int[tableDimensions];
                firstPhaseRestarts[0] = 1;
                for (int i=1;i<firstPhaseRestarts.length;i++) {
                    xAxisFactor = Math.pow ( targetNumberRightCorner / (double)firstPhaseRestarts[i-1], 1d/(tableDimensions-i));
    //                System.out.println("xAxisFactor="+xAxisFactor);
                    firstPhaseRestarts[i] = (int)  Math.round( xAxisFactor *firstPhaseRestarts[i-1] ) ;// + adjustmentOffset;

                    // a check to prevent twice the same combination
                    if (i>=1 && firstPhaseRestarts[i]<=firstPhaseRestarts[i-1]) {
                        firstPhaseRestarts[i] = firstPhaseRestarts[i-1]+1;// increment by one if [i] would be identical to [i-1]
                    }
                }
    //            System.out.println(Arrays.toString(firstPhaseRestarts));

                // for cross domain study, overwrite firstPhaseRestarts since pre-defined steps are used instead of the generic double-logarithmic
                if (doCrossDomainStudy) {
                    switch(tfactor) {
                        case 100:
                            firstPhaseRestarts = new int[]{1,4,10};
                            break;
                        case 400:
                            firstPhaseRestarts = new int[]{1,4,10,40};
                            break;
                        case 1000:
                            firstPhaseRestarts = new int[]{1,4,10,40,100};
                            break;
                        case 4000:
                            firstPhaseRestarts = new int[]{1,4,10,40,100,400};
                            break;
                        default:
                            break;
                    }
                }

//                int sanityCheck=0;
                for (int kk=0; kk<firstPhaseRestarts.length; kk++) {
                    int k=firstPhaseRestarts[kk];

                    for (int ll=0; ll<firstPhaseRestarts.length; ll++) {
                        int l=firstPhaseRestarts[ll];

                        if (kk==0 && ll==0) {
                            // speedup for the cluster, as otherwise job "1" (the first one) is waisting a lot of time (since we run completely from scratch)
                            System.out.println(base+" "+instance+" "+
                                    totalTimeBudget+" "+
                                    k+" "+tinit+" "+
                                    l+" "+tinit+" "+
                                    reps+" false");
                        } else {
                            System.out.println(base+" "+instance+" "+
                                    totalTimeBudget+" "+
                                    k+" "+(totalTimeInFirstPhase/k)+" "+
                                    l+" "+(totalTimeInSecondPhase/l)+" "+
                                    reps+" false");
                        }
//                        sanityCheck++;
                    }
                }
    //            System.out.println("sanityCheck="+sanityCheck);
            }
        }
    System.exit(0); // to prevent other functions from running afterwards
    }
    
    public static String[] ttpInstances = new String[]{
        "eil51_n150_bounded-strongly-corr_03.ttp",  "eil51_n500_uncorr_07.ttp",
        "eil76_n225_bounded-strongly-corr_03.ttp",  "eil76_n750_uncorr_07.ttp",
        "kroA100_n297_bounded-strongly-corr_03.ttp","kroA100_n990_uncorr_07.ttp",
        "u159_n474_bounded-strongly-corr_03.ttp",   "u159_n1580_uncorr_07.ttp",
        "ts225_n672_bounded-strongly-corr_03.ttp",  "ts225_n2240_uncorr_07.ttp",
    };
    public static double[] ttpInits     = new double[]{3,3,3,3,3,3,3,3,3,3}; // based on the largest instance without boosting
//    public static String[] ttpInstances = new String[]{"ts225_n2240_uncorr_07.ttp"};
//    public static String[] ttpInstances = new String[]{"eil51_n150_bounded-strongly-corr_03.ttp"};
//    public static double[] ttpInits     = new double[]{3};
    
    /* TSP instances
        9 largest from TSPlib plus mona-lisa100K 
    */
    public static String[] tspInstances = new String[]{"rl5934.tsp","pla7397.tsp","rl11849.tsp","usa13509.tsp","brd14051.tsp",
                                        "d15112.tsp","d18512.tsp","pla33810.tsp","pla85900.tsp","mona-lisa100K.tsp"};
    public static double[] tspInits     = new double[]{0.3,0.3,0.3,0.3,0.3,
                                        0.3,0.3,0.5,2.5,2.5};
//    public static String[] tspInstances = new String[]{"pla33810.tsp",};
//    public static double[] tspInits     = new double[]{0.5};
    
    /* MVC instances
    large-rec-amazon.mtx.47606.mis           49 better than numvc in IJCAI 2015 paper
    large-soc-digg.mtx.103244.mis            59
    large-web-it-2004.mtx.414671.mis         67
    large-soc-youtube-snap.mtx.276945.mis    70
    large-soc-flickr.mtx.153272.mis          71
    large-ca-coauthors-dblp.mtx.472179.mis   72
    large-soc-youtube.mtx.146376.mis         80
    large-soc-gowalla.mtx.84222.mis          91
    large-sc-shipsec5.mtx.147137.mis         151
    large-sc-shipsec1.mtx.117318.mis         159
    //not used: large-tech-RL-caida.mtx.74930.mis   -171 (worse than numvc)
    */
    public static String[] mvcInstances = new String[]{"large-rec-amazon.mtx.47606.mis","large-soc-gowalla.mtx.84222.mis","large-soc-digg.mtx.103244.mis",
                                           "large-sc-shipsec1.mtx.117318.mis","large-soc-youtube.mtx.146376.mis","large-sc-shipsec5.mtx.147137.mis",
                                           "large-soc-flickr.mtx.153272.mis","large-soc-youtube-snap.mtx.276945.mis","large-web-it-2004.mtx.414671.mis",
                                           "large-ca-coauthors-dblp.mtx.472179.mis"};
    public static double[] mvcInits    = new double[]{1.5,1.5,1.5,1.5,1.5,
                                           1.5,1.5,1.5,1.5,1.5};
//    public static String[] mvcInstances = new String[]{"large-sc-shipsec1.mtx.117318.mis"};
//    public static double[] mvcInits    = new double[]{1.5};
    
    /* MAXSAT instances
    see additionalInfo.txt, slowest 10
    */
    public static String[] maxsatInstances = new String[]{
        "UI-dividers10.dimacs.filtered.457.cnf","UI-wb1.dimacs.filtered.750.cnf","UC-scpcyc09_maxsat.788.cnf",
        "UC-scpcyc10_maxsat.1850.cnf","UC-scpcyc11_maxsat.4020.cnf",
        "UI-mem_ctrl2_blackbox_mc_dp-problem.dimacs_28.filtered.992760.cnf",
        "UI-mem_ctrl-problem.dimacs_27.filtered.2541789.cnf",
        "UI-wb_4m8s-problem.dimacs_47.filtered.1427039.cnf","UI-wb_4m8s-problem.dimacs_48.filtered.1472549.cnf","UI-wb_4m8s-problem.dimacs_49.filtered.1482446.cnf"};
    public static double[] maxsatInits    = new double[]{ 1.5,1.5,1.5,
                                            1.5,1.5,
                                            30,
                                            35,
                                            200,285,295};
//    public static String[] maxsatInstances = new String[]{"UC-scpcyc11_maxsat.4020.cnf"};
//    public static double[] maxsatInits    = new double[]{1.5};
       
    
    public static void generateArray() {
        String base = "/home/mwagner/scratch/jdk1.8.0_05/bin/java -classpath build/classes/ restarts.Driver mona-lisa100K.tsp";
        base = "/home/mwagner/scratch/jdk1.8.0_05/bin/java -classpath build/classes/ restarts.Driver large-sc-shipsec1.mtx.117318.mis";
        base = "/home/mwagner/scratch/jdk1.8.0_05/bin/java -classpath build/classes/ restarts.Driver ts225_n2240_uncorr_07.ttp"; // 3s init
        base = "/home/mwagner/scratch/jdk1.8.0_05/bin/java -classpath build/classes/ restarts.Driver eil51_n150_bounded-strongly-corr_03.ttp"; // 3s init
//        base = "/home/mwagner/scratch/jdk1.8.0_05/bin/java -classpath build/classes/ restarts.Driver UR-HG-3SAT-V250-C1000-21.cnf";
        
        double totalTime = 1200;
        double firstPhaseTime = 3; 
        /* 0.5 for pla33810.tsp on MMCI, 
           2.5 for mona-lisa100K.tsp on MMCI
        
           1.5 for large-sc-shipsec1.mtx.117318.mis on MPI
        
           1.5 for UR-HG-3SAT-V250-C1000-21.cnf on MPI
           1.5 for UC-scpcyc11_maxsat.4020.cnf on MPI
        
           3.0 for ts225_n2240_uncorr_07.ttp on MPI
        */
        
        int overallExperimentRepetitions = 100;
        int tableDimensions = 30;
        
        boolean doLuby = false;
        
        
//        double tableFactor = 1.2;
        
        
        /* this way, only half or so of the combinations will be valid, which has the advantage that we cover the entire bottom left triangle
        note: the formula
        */
        double tableFactor = Math.pow ( totalTime/firstPhaseTime, 1d/(tableDimensions-1)); 
        
//        System.out.println("tableFactor="+tableFactor);
        
        double lubyFactorForLubySequenceLength = 0;
        if (doLuby) {
            int tooLargeIndex = 0;
            for (int i=0; i<lubySum.length; i++) {
                if (lubySum[i]*firstPhaseTime > totalTime) {
                    tooLargeIndex = i;
                    break;
                }
            }
            lubyFactorForLubySequenceLength = Math.pow ( tooLargeIndex, 1d/(tableDimensions-1)); 
        }
        
        
        int targetNumberRightCorner = (int) (totalTime / firstPhaseTime);
        double xAxisFactor = Math.pow ( targetNumberRightCorner, 1d/(tableDimensions-1)); // -1 since I know the very first value
//        System.out.println("xAxisFactor="+xAxisFactor);
        
        // restarts array
        int[] firstPhaseRestarts = new int[tableDimensions];
        int indexCounter = 0;
//        int adjustmentCounter = 0;
//        int adjustmentOffset = 1;
        firstPhaseRestarts[0] = 1;
//        for (int i=0;i<firstPhaseRestarts.length;i++) {
        for (int i=1;i<firstPhaseRestarts.length;i++) {
//            for (int i=firstPhaseRestarts.length-1;i>0;i--) {
            
            // the following two lines: non-Luby and Luby
//            firstPhaseRestarts[i] =             (int) Math.round( Math.pow(xAxisFactor,i) );
//            System.out.println("i="+i+" firstPhaseRestarts[i-1]="+firstPhaseRestarts[i-1]);
//            System.out.println(targetNumberRightCorner / (double)firstPhaseRestarts[i-1] +" "+(tableDimensions-i) );
            
            xAxisFactor = Math.pow ( targetNumberRightCorner / (double)firstPhaseRestarts[i-1], 1d/(tableDimensions-i));
            
//            System.out.println("xAxisFactor="+xAxisFactor);
//            
            firstPhaseRestarts[i] =             (int)  Math.round( xAxisFactor *firstPhaseRestarts[i-1] ) ;// + adjustmentOffset;
            if (doLuby) firstPhaseRestarts[i] = (int) Math.ceil( Math.pow(lubyFactorForLubySequenceLength,i) );
            

            
//            adjustmentCounter++;
            // a check to prevent twice the same combination
            if (i>=1 && firstPhaseRestarts[i]<=firstPhaseRestarts[i-1]) {
                firstPhaseRestarts[i] = firstPhaseRestarts[i-1]+1;/*orig*/
            }
//            xAxisFactor = Math.pow ( targetNumberRightCorner-firstPhaseRestarts[i], 1d/(tableDimensions-(i+1)-1));
//            System.out.println("xAxisFactor="+xAxisFactor+" adjustmentOffset="+adjustmentOffset+" adjustmentCounter="+adjustmentCounter+"\n");
//            if (i<firstPhaseRestarts.length-1 && firstPhaseRestarts[i]>=firstPhaseRestarts[i+1]) firstPhaseRestarts[i] = firstPhaseRestarts[i+1]-1;
//            System.out.println(Arrays.toString(firstPhaseRestarts));
//                        System.out.println(firstPhaseRestarts[i]+" -> "+Arrays.toString(firstPhaseRestarts));
        }
        
        double[] firstPhaseTimes = new double[tableDimensions];
        indexCounter = 0;
        for (double d=firstPhaseTime;;d=d*tableFactor) {
            firstPhaseTimes[indexCounter] = d;
            indexCounter++;
            if (indexCounter==firstPhaseTimes.length) break;
        }
        
        for (int i=0; i<firstPhaseRestarts.length; i++) {
            for (int j=0; j<firstPhaseTimes.length; j++) {
                System.out.println(base+" "+totalTime+" "+firstPhaseRestarts[i]+" "+firstPhaseTimes[j]+" "+overallExperimentRepetitions+" "+doLuby);
            }
        }
        System.exit(0);
    }
    
    public static int numberOfRepetitions = 20;
    
    /*
    P1: restart phase
    P2: subsequent phase in which "best of P1" is used
    
    ,0 means don't do Luby
    ,1 means do Luby
    */
    public static double budgetFactor = 100;
    public static double[][] crossStudyConfigs = new double[][]{
        
//        //10*t_init
//        {1*budgetFactor/10,1,budgetFactor/100/10/*going <1/100th does not matter here*/,numberOfRepetitions,0},    // 1 regular run, due to the framework I need to do one quick restart initially with the full time then (==P2 only)
//        {1*budgetFactor/10,4,budgetFactor/4/10,numberOfRepetitions,0},  // 4 regular runs, each with 25% (==P1 only)
//        {1*budgetFactor/10,4,budgetFactor/10/10,numberOfRepetitions,0}, // P1: 4 runs, each with 10%, then P2
////        {1*budgetFactor,10,budgetFactor/25,numberOfRepetitions,0}, // P1: 10 runs, each with 2.5% --> 4%!, then P2
////        {1*budgetFactor,40,budgetFactor/100,numberOfRepetitions,0}, // P1: 40 runs, each with 1%, then P
////        {1*budgetFactor,4,budgetFactor/40,numberOfRepetitions,0}, // P1: 4 runs, each with 2.5%, then P2
////        {1*budgetFactor,10,budgetFactor/100,numberOfRepetitions,0}, // P1: 10 runs, each with 1%, then P2
/////*400*/{1*budgetFactor,40,budgetFactor/400,numberOfRepetitions,0}, // P1: 40 regular runs, each with 0.25%, then P2
////        {1*budgetFactor,4,budgetFactor/100,numberOfRepetitions,0}, // P1: 4 runs, each with 1%, then P2
/////*400*/{1*budgetFactor,10,budgetFactor/400,numberOfRepetitions,0}, // P1: 10 runs, each with 0.25%, then P2
/////*1000*/{1*budgetFactor,40,budgetFactor/1000,numberOfRepetitions,0}, // P1: 40 regular runs, each with 0.1%, then P2 
////        {1*budgetFactor/10,4,budgetFactor/100/10/*not defined*/,numberOfRepetitions,1}, // P1: Luby with sequence length 4 (5 units in total), then P2
////        {1*budgetFactor,10,budgetFactor/100,numberOfRepetitions,1}, // P1: 16 Luby units in total, then P2
////        {1*budgetFactor,40,budgetFactor/100,numberOfRepetitions,1}, // P1: 94 Luby units in total, then P2
/////*400*/{1*budgetFactor,100,budgetFactor/100,numberOfRepetitions,1}, // P1: 280 Luby units in total, then P2
//        
//        //40*t_init
//        {1*budgetFactor/2.5,1,budgetFactor/100/2.5/*going <1/100th does not matter here*/,numberOfRepetitions,0},    // 1 regular run, due to the framework I need to do one quick restart initially with the full time then (==P2 only)
//        {1*budgetFactor/2.5,4,budgetFactor/4/2.5,numberOfRepetitions,0},  // 4 regular runs, each with 25% (==P1 only)
//        {1*budgetFactor/2.5,4,budgetFactor/10/2.5,numberOfRepetitions,0}, // P1: 4 runs, each with 10%, then P2
//        {1*budgetFactor/2.5,10,budgetFactor/25/2.5,numberOfRepetitions,0}, // P1: 10 runs, each with 2.5% --> 4%!, then P2
////        {1*budgetFactor,40,budgetFactor/100,numberOfRepetitions,0}, // P1: 40 runs, each with 1%, then P
//        {1*budgetFactor/2.5,4,budgetFactor/40/2.5,numberOfRepetitions,0}, // P1: 4 runs, each with 2.5%, then P2
////        {1*budgetFactor,10,budgetFactor/100,numberOfRepetitions,0}, // P1: 10 runs, each with 1%, then P2
/////*400*/{1*budgetFactor,40,budgetFactor/400,numberOfRepetitions,0}, // P1: 40 regular runs, each with 0.25%, then P2
////        {1*budgetFactor,4,budgetFactor/100,numberOfRepetitions,0}, // P1: 4 runs, each with 1%, then P2
/////*400*/{1*budgetFactor,10,budgetFactor/400,numberOfRepetitions,0}, // P1: 10 runs, each with 0.25%, then P2
/////*1000*/{1*budgetFactor,40,budgetFactor/1000,numberOfRepetitions,0}, // P1: 40 regular runs, each with 0.1%, then P2 
////        {1*budgetFactor/10,4,budgetFactor/100/10/*not defined*/,numberOfRepetitions,1}, // P1: Luby with sequence length 4 (5 units in total), then P2
////        {1*budgetFactor,10,budgetFactor/100,numberOfRepetitions,1}, // P1: 16 Luby units in total, then P2
////        {1*budgetFactor,40,budgetFactor/100,numberOfRepetitions,1}, // P1: 94 Luby units in total, then P2
/////*400*/{1*budgetFactor,100,budgetFactor/100,numberOfRepetitions,1}, // P1: 280 Luby units in total, then P2
        
        //100*t_init
        {1*budgetFactor,1,budgetFactor/100,numberOfRepetitions,0},    // 1 regular run, due to the framework I need to do one quick restart initially with the full time then (==P2 only)
        {1*budgetFactor,4,budgetFactor/4,numberOfRepetitions,0},  // 4 regular runs, each with 25% (==P1 only)
        {1*budgetFactor,4,budgetFactor/10,numberOfRepetitions,0}, // P1: 4 runs, each with 10%, then P2
        {1*budgetFactor,10,budgetFactor/25,numberOfRepetitions,0}, // P1: 10 runs, each with 2.5% --> 4%!, then P2
        {1*budgetFactor,40,budgetFactor/100,numberOfRepetitions,0}, // P1: 40 runs, each with 1%, then P
        {1*budgetFactor,4,budgetFactor/40,numberOfRepetitions,0}, // P1: 4 runs, each with 2.5%, then P2
        {1*budgetFactor,10,budgetFactor/100,numberOfRepetitions,0}, // P1: 10 runs, each with 1%, then P2
///*400*/{1*budgetFactor,40,budgetFactor/400,numberOfRepetitions,0}, // P1: 40 regular runs, each with 0.25%, then P2
        {1*budgetFactor,4,budgetFactor/100,numberOfRepetitions,0}, // P1: 4 runs, each with 1%, then P2
///*400*/{1*budgetFactor,10,budgetFactor/400,numberOfRepetitions,0}, // P1: 10 runs, each with 0.25%, then P2
///*1000*/{1*budgetFactor,40,budgetFactor/1000,numberOfRepetitions,0}, // P1: 40 regular runs, each with 0.1%, then P2 
        {1*budgetFactor,4,budgetFactor/100,numberOfRepetitions,1}, // P1: Luby with sequence length 4, then P2
        {1*budgetFactor,10,budgetFactor/100,numberOfRepetitions,1}, // P1: 16 Luby units in total, then P2
        {1*budgetFactor,40,budgetFactor/100,numberOfRepetitions,1}, // P1: 94 Luby units in total, then P2
///*400*/{1*budgetFactor,100,budgetFactor/100,numberOfRepetitions,1}, // P1: 280 Luby units in total, then P2
        
        //400*t_init
        {4*budgetFactor,1,4*budgetFactor/100,numberOfRepetitions,0},    // 1 regular run, due to the framework I need to do one quick restart initially with the full time then (==P2 only)
        {4*budgetFactor,4,4*budgetFactor/4,numberOfRepetitions,0},  // 4 regular runs, each with 25% (==P1 only)
        {4*budgetFactor,4,4*budgetFactor/10,numberOfRepetitions,0}, // P1: 4 runs, each with 10%, then P2
        {4*budgetFactor,10,4*budgetFactor/25,numberOfRepetitions,0}, // P1: 10 runs, each with 2.5% --> 4%!, then P2
        {4*budgetFactor,40,4*budgetFactor/100,numberOfRepetitions,0}, // P1: 40 runs, each with 1%, then P
        {4*budgetFactor,4,4*budgetFactor/40,numberOfRepetitions,0}, // P1: 4 runs, each with 2.5%, then P2
        {4*budgetFactor,10,4*budgetFactor/100,numberOfRepetitions,0}, // P1: 10 runs, each with 1%, then P2
/*400*/{4*budgetFactor,40,4*budgetFactor/400,numberOfRepetitions,0}, // P1: 40 regular runs, each with 0.25%, then P2
        {4*budgetFactor,4,4*budgetFactor/100,numberOfRepetitions,0}, // P1: 4 runs, each with 1%, then P2
/*400*/{4*budgetFactor,10,4*budgetFactor/250,numberOfRepetitions,0}, // P1: 10 runs, each with 0.25% --> 0.4%!, then P2
///*1000*/{1*budgetFactor,40,budgetFactor/1000,numberOfRepetitions,0}, // P1: 40 regular runs, each with 0.1%, then P2 
        {4*budgetFactor,4,4*budgetFactor/100,numberOfRepetitions,1}, // P1: Luby with sequence length 4, then P2
        {4*budgetFactor,10,4*budgetFactor/100,numberOfRepetitions,1}, // P1: 16 Luby units in total, then P2
        {4*budgetFactor,40,4*budgetFactor/100,numberOfRepetitions,1}, // P1: 94 Luby units in total, then P2
/*400*/{4*budgetFactor,100,4*budgetFactor/100,numberOfRepetitions,1}, // P1: 280 Luby units in total, then P2 --> logical problem: result will be "Infinity: since the smallest time unit is 1/100th, not 1/400 or 1/1000 here! :-(
        
        //1000*t_init
        {10*budgetFactor,1,10*budgetFactor/100,numberOfRepetitions,0},    // 1 regular run, due to the framework I need to do one quick restart initially with the full time then (==P2 only)
        {10*budgetFactor,4,10*budgetFactor/4,numberOfRepetitions,0},  // 4 regular runs, each with 25% (==P1 only)
        {10*budgetFactor,4,10*budgetFactor/10,numberOfRepetitions,0}, // P1: 4 runs, each with 10%, then P2
        {10*budgetFactor,10,10*budgetFactor/25,numberOfRepetitions,0}, // P1: 10 runs, each with 2.5% --> 4%!, then P2
        {10*budgetFactor,40,10*budgetFactor/100,numberOfRepetitions,0}, // P1: 40 runs, each with 1%, then P
        {10*budgetFactor,4,10*budgetFactor/40,numberOfRepetitions,0}, // P1: 4 runs, each with 2.5%, then P2
        {10*budgetFactor,10,10*budgetFactor/100,numberOfRepetitions,0}, // P1: 10 runs, each with 1%, then P2
/*400*/{10*budgetFactor,40,10*budgetFactor/400,numberOfRepetitions,0}, // P1: 40 regular runs, each with 0.25%  --> 0.45!, then P2
        {10*budgetFactor,4,10*budgetFactor/100,numberOfRepetitions,0}, // P1: 4 runs, each with 1%, then P2
/*400*/{10*budgetFactor,10,10*budgetFactor/250,numberOfRepetitions,0}, // P1: 10 runs, each with 0.25% --> 0.4%!, then P2
/*1000*/{10*budgetFactor,40,10*budgetFactor/1000,numberOfRepetitions,0}, // P1: 40 regular runs, each with 0.1%, then P2 
        {10*budgetFactor,4,10*budgetFactor/100,numberOfRepetitions,1}, // P1: Luby with sequence length 4, then P2
        {10*budgetFactor,10,10*budgetFactor/100,numberOfRepetitions,1}, // P1: 16 Luby units in total, then P2
        {10*budgetFactor,40,10*budgetFactor/100,numberOfRepetitions,1}, // P1: 94 Luby units in total, then P2
/*400*/{10*budgetFactor,100,10*budgetFactor/100,numberOfRepetitions,1}, // P1: 280 Luby units in total, then P2 --> logical problem: result will be "Infinity: since the smallest time unit is 1/100th, not 1/400 or 1/1000 here! :-(

//        //4000*t_init
//        {40*budgetFactor,1,40*budgetFactor/100,numberOfRepetitions,0},    // 1 regular run, due to the framework I need to do one quick restart initially with the full time then (==P2 only)
//        {40*budgetFactor,4,40*budgetFactor/4,numberOfRepetitions,0},  // 4 regular runs, each with 25% (==P1 only)
//        {40*budgetFactor,4,40*budgetFactor/10,numberOfRepetitions,0}, // P1: 4 runs, each with 10%, then P2
//        {40*budgetFactor,10,40*budgetFactor/25,numberOfRepetitions,0}, // P1: 10 runs, each with 2.5% --> 4%!, then P2
//        {40*budgetFactor,40,40*budgetFactor/100,numberOfRepetitions,0}, // P1: 40 runs, each with 1%, then P
//        {40*budgetFactor,4,40*budgetFactor/40,numberOfRepetitions,0}, // P1: 4 runs, each with 2.5%, then P2
//        {40*budgetFactor,10,40*budgetFactor/100,numberOfRepetitions,0}, // P1: 10 runs, each with 1%, then P2
///*400*/{40*budgetFactor,40,40*budgetFactor/400,numberOfRepetitions,0}, // P1: 40 regular runs, each with 0.25%  --> 0.45!, then P2
//        {40*budgetFactor,4,40*budgetFactor/100,numberOfRepetitions,0}, // P1: 4 runs, each with 1%, then P2
///*400*/{40*budgetFactor,10,40*budgetFactor/250,numberOfRepetitions,0}, // P1: 10 runs, each with 0.25% --> 0.4%!, then P2
///*1000*/{40*budgetFactor,40,40*budgetFactor/1000,numberOfRepetitions,0}, // P1: 40 regular runs, each with 0.1%, then P2 
//        {40*budgetFactor,4,40*budgetFactor/100,numberOfRepetitions,1}, // P1: Luby with sequence length 4, then P2
//        {40*budgetFactor,10,40*budgetFactor/100,numberOfRepetitions,1}, // P1: 16 Luby units in total, then P2
//        {40*budgetFactor,40,40*budgetFactor/100,numberOfRepetitions,1}, // P1: 94 Luby units in total, then P2
/////*400*/{10*budgetFactor,100,10*budgetFactor/100,numberOfRepetitions,1}, // P1: 280 Luby units in total, then P2 --> logical problem: result will be "Infinity: since the smallest time unit is 1/100th, not 1/400 or 1/1000 here! :-(
//
//        //10000*t_init
//        {100*budgetFactor,1,100*budgetFactor/100,numberOfRepetitions,0},    // 1 regular run, due to the framework I need to do one quick restart initially with the full time then (==P2 only)
//        {100*budgetFactor,4,100*budgetFactor/4,numberOfRepetitions,0},  // 4 regular runs, each with 25% (==P1 only)
//        {100*budgetFactor,4,100*budgetFactor/10,numberOfRepetitions,0}, // P1: 4 runs, each with 10%, then P2
//        {100*budgetFactor,10,100*budgetFactor/25,numberOfRepetitions,0}, // P1: 10 runs, each with 2.5% --> 4%!, then P2
//        {100*budgetFactor,40,100*budgetFactor/100,numberOfRepetitions,0}, // P1: 40 runs, each with 1%, then P
//        {100*budgetFactor,4,100*budgetFactor/40,numberOfRepetitions,0}, // P1: 4 runs, each with 2.5%, then P2
//        {100*budgetFactor,10,100*budgetFactor/100,numberOfRepetitions,0}, // P1: 10 runs, each with 1%, then P2
///*400*/{100*budgetFactor,40,100*budgetFactor/400,numberOfRepetitions,0}, // P1: 40 regular runs, each with 0.25%  --> 0.45!, then P2
//        {100*budgetFactor,4,100*budgetFactor/100,numberOfRepetitions,0}, // P1: 4 runs, each with 1%, then P2
///*400*/{100*budgetFactor,10,100*budgetFactor/250,numberOfRepetitions,0}, // P1: 10 runs, each with 0.25% --> 0.4%!, then P2
///*1000*/{100*budgetFactor,40,100*budgetFactor/1000,numberOfRepetitions,0}, // P1: 40 regular runs, each with 0.1%, then P2 
//        {100*budgetFactor,4,100*budgetFactor/100,numberOfRepetitions,1}, // P1: Luby with sequence length 4, then P2
//        {100*budgetFactor,10,100*budgetFactor/100,numberOfRepetitions,1}, // P1: 16 Luby units in total, then P2
//        {100*budgetFactor,40,100*budgetFactor/100,numberOfRepetitions,1}, // P1: 94 Luby units in total, then P2
/////*400*/{10*budgetFactor,100,10*budgetFactor/100,numberOfRepetitions,1}, // P1: 280 Luby units in total, then P2 --> logical problem: result will be "Infinity: since the smallest time unit is 1/100th, not 1/400 or 1/1000 here! :-(
    };
    
    public static void generateArrayForCrossStudy() {
        String base = "/home/mwagner/scratch/jdk1.8.0_05/bin/java -classpath build/classes/ restarts.Driver";
        
        String[] targetInstances = ttpInstances;
        double[] targetTimes = ttpInits;
//        String[] targetInstances = tspInstances;
//        double[] targetTimes = tspInits;
//        String[] targetInstances = mvcInstances;
//        double[] targetTimes = mvcInits;
//        String[] targetInstances = maxsatInstances;
//        double[] targetTimes = maxsatInits;
        
        for (int j=0; j<targetInstances.length; j++) {
            String instance = targetInstances[j];
            double time = targetTimes[j];
                    
            for (int i=0; i<crossStudyConfigs.length; i++) {
                boolean doLuby = (crossStudyConfigs[i][4]==0)?false:true;
                
                double reps = crossStudyConfigs[i][3];
                
                if (time > 29) reps = 1; // for all those that take long --> do just 1 repetition
                
                // OLD: System.out.println(base+"       "+totalTime+"                         "+firstPhaseRestarts[i]+"         "+firstPhaseTimes[j]+"        "+overallExperimentRepetitions+" "+doLuby);
                System.out.println(base+" "+instance+" "+(crossStudyConfigs[i][0]*time)+" "+((int)crossStudyConfigs[i][1])+" "+ (crossStudyConfigs[i][2]*time) +" "+ ((int)reps) +" "+doLuby);
                
            }
        }
        System.exit(0);
    }
    
    
    
    
    
    public static void commandLineVersion(String[] args) {
                
        if (args.length!=6 /*single stage*/ && args.length!=8 /*two stage*/) {
            System.out.println("EXIT: commandLineVersion, wrong number of arguments ("+args.length+")");
            System.exit(0);
        }
        
        Problem p = null;
        int problemType = 0;
                
        if (args[0].endsWith(".tsp")) {
            p = new TSP(args[0]);
            problemType = 2;                      // this is needed for TSP if Linkern is default
        } else if (args[0].endsWith(".mis")) {
            p = new MVC(args[0]);
            problemType = 3;                      // this is needed for MVC if NUMVC is default
        } else if (args[0].endsWith(".cnf")) {
            p = new MAXSAT(args[0]);
            problemType = 4;                      // this is needed for MAXSAT if CCLS is default
        } else if (args[0].endsWith(".ttp")) {
            p = new TTP(args[0]);
            problemType = 5;                      // this is needed for TTP if ACOTSPJavaTTP is default
        } else {
            System.out.println("EXIT: commandLineVersion, unknown problem type");
            System.exit(0);
        }
        
        // single stage:
        double totalTime = Double.parseDouble(args[1]);
        int firstPhaseRestarts = Integer.parseInt(args[2]);
        double firstPhaseTime = Double.parseDouble(args[3]);
        
        int secondPhaseRestarts=0;
        double secondPhaseTime=0;
        if (args.length==8) {
            secondPhaseRestarts = Integer.parseInt(args[4]);
            secondPhaseTime = Double.parseDouble(args[5]);
        }
        
        int overallExperimentRepetitions = Integer.parseInt(args[args.length-2]);
        boolean doLubyInFirstPhase = Boolean.parseBoolean(args[args.length-1]);
        
        boolean showIndividualResults = true;
        
        double result = repeatExperiments(p, 
                        totalTime, 
                        firstPhaseRestarts, firstPhaseTime, 
                        secondPhaseRestarts, secondPhaseTime, 
                        overallExperimentRepetitions,
                        problemType, doLubyInFirstPhase, 
                        showIndividualResults);
                    
        // last line printed to STDOUT: average of all the runs performed
        if (secondPhaseRestarts==0) { 
            // means: no second inner phase before the last phase
            System.out.println(firstPhaseRestarts+","+firstPhaseTime+","+result);
        } else {
            System.out.println(firstPhaseRestarts+","+firstPhaseTime+","+secondPhaseRestarts+","+secondPhaseTime+","+result);
        }
    }
    
    public static void main(String[] args) {    
        
//        generateArrayForCrossStudy();
        
//        System.out.println(Arrays.toString(lubySum));
        
        if (args.length==0) {
            generateArray();
//            generateArrayForCrossStudy();
//            generateArrayForKLStudy();
            
//            commandLineVersion(new String[]{"rl5934.tsp","10.0","10","0.3","4","0.9","2","false"});
//            commandLineVersion(new String[]{"eil51_n150_bounded-strongly-corr_03.ttp","10","1","3","1","3","20","false"});
//            commandLineVersion(new String[]{"ts225_n2240_uncorr_07.ttp","10","2","3","1","false"});
            commandLineVersion(new String[]{"ts225_n2240_uncorr_07.ttp","20","2","3","1","3","1","false"});
        }
        
        if (args.length!=0) {
            commandLineVersion(args);
            System.exit(0);
        } 
//        
//        boolean doLubyInFirstPhase = false;
//        
//        Problem p = new TwoMax(100);
//        int problemType = 1;
//        double totalTime = 400; // depending on the algorithm, this is intepreted as "evaluations" or as "time in seconds"
//        int overallExperimentRepetitions = 100;
//        
////        p = new TSP("d15112.tsp"); // 13 seconds 1575794, 4 seconds 1577516
//        p = new TSP("pla33810.tsp"); 
////        p = new TSP("pla85900.tsp"); // 1 seconds 143 900 000, 10 seconds 142 900 000, 60 seconds 142 700 000, 75 seconds 142 650000/142 750000
////        p = new TSP("fl3795.tsp");// quite short
//        totalTime = 5;
//        overallExperimentRepetitions = 1;
//        
//        
//        p = new MVC("frb100-40.mis");
////        p = new MVC("large-rec-amazon.mtx.47606.mis");
//        
//        problemType = ((p instanceof TSP)?2:problemType) ; // this is needed for TSP
//        problemType = ((p instanceof MVC)?3:problemType) ; // this is needed for MVC
//        problemType = ((p instanceof MAXSAT)?4:problemType) ; // this is needed for MAXSAT
//        
//        
//        if (!true) {
////            int firstPhaseRestarts = 5; //x
////            int firstPhaseEvaluations = 1; //y
////            repeatExperiments(p, 
////                    totalTime, firstPhaseRestarts, firstPhaseEvaluations, 
////                    overallExperimentRepetitions);
//            
//            
//        } else {
//            System.out.println("firstPhaseRestarts,firstPhaseTime,result");
//            int counter = 0; // used to populate the following table
//
//            double[][] allResults = new double
//                    //[((restartsEnd-restartsBegin)/restartsInterval+1) * (evaluationsEnd+1)]
////                    [(evaluationsEnd+1) * (evaluationsEnd+1)]
//                    [6*6]
//                    [3];
//            
//            int[] restartsArray = new int[]{1,2,4,7,15,30};
//            for (int i=0; 
////                    firstPhaseRestarts<=restartsEnd; 
////                    firstPhaseRestarts=firstPhaseRestarts+restartsInterval) { // restarts
/////**/                    firstPhaseRestarts<=Math.pow(2,6); 
////                    firstPhaseRestarts=(int) Math.ceil( firstPhaseRestarts*1.2 ) )  { // restarts
//                    i<restartsArray.length; 
//                    i++  )  { // restarts
//                int firstPhaseRestarts = restartsArray[i];
//                
////                for (double firstPhaseTime=1; 
////                        firstPhaseTime<=Math.pow(2,9); 
////                        firstPhaseTime=Math.ceil( firstPhaseTime*1.2 )  ) { // evals
//                
//                for (double firstPhaseTime=0.2; 
//                        firstPhaseTime<= 0.2*Math.pow(2,5); 
//                        firstPhaseTime=  firstPhaseTime*2
//                        ) { // evals
//                    
//                    double result = repeatExperiments(p, 
//                        totalTime, firstPhaseRestarts, firstPhaseTime, 
//                        -1, -1, 
//                        overallExperimentRepetitions,
//                        problemType, doLubyInFirstPhase,
//                        true);
//                    
//                    System.out.println(firstPhaseRestarts+","+firstPhaseTime+","+result);
//                    
//                    allResults[counter][0]=firstPhaseRestarts;
//                    allResults[counter][1]=firstPhaseTime;
//                    allResults[counter][2]=result;
//                    counter++;
//                }
//            }
//            // determine max/min fitness
//            double maxFitness = Double.NEGATIVE_INFINITY;
//            double minFitness = Double.POSITIVE_INFINITY;
//            for (int i=0; i<allResults.length; i++)
//            {   
//                if (allResults[i][2]>maxFitness) maxFitness = allResults[i][2];
//                if (allResults[i][2]<minFitness) minFitness = allResults[i][2];
//            }
//            
//            System.out.println("firstPhaseRestarts,firstPhaseTime,result");
//            double[][] allResultsMaximisation = (double[][]) DeepCopy.copy(allResults);
//            double[][] allResultsMinimisation = (double[][]) DeepCopy.copy(allResults);
//            if (p.isMaximisation) 
//                for (int i=0; i<allResults.length; i++)
//                {
//                    // the following lines are for maximisation problems (e.g. OneMax, TwoMax, ...)
//                    allResultsMaximisation[i][2] = 100*(1d-allResultsMaximisation[i][2]/maxFitness); // distance in percent to maximum fitness
//                    System.out.println(allResultsMaximisation[i][0]+","+allResultsMaximisation[i][1]+","+allResultsMaximisation[i][2]);
//                }
//            else
//                for (int i=0; i<allResults.length; i++)
//                {
//                    // the following lines are for minimisation problems (e.g. TSP, ...)
//                    allResultsMinimisation[i][2] = 100*(allResultsMinimisation[i][2]/minFitness -1d); // distance in percent to minimum fitness
//                    if (allResultsMinimisation[i][2] == Double.POSITIVE_INFINITY) allResultsMinimisation[i][2] = 100; // 100%
//                    System.out.println(allResultsMinimisation[i][0]+","+allResultsMinimisation[i][1]+","+allResultsMinimisation[i][2]);
//                }
//        }
    }
    
    public static double repeatExperiments(Problem p, 
            double totalTime, 
            int firstPhaseRestarts, double firstPhaseTime, 
            int secondPhaseRestarts, double secondPhaseTime, 
            int overallExperimentRepetitions,
            int problemType, boolean doLubyInFirstPhase,
            boolean showIndividualResults) {
        boolean debugPrint = false;
        
        if (totalTime < firstPhaseTime*firstPhaseRestarts-0.000001 ||
                secondPhaseRestarts > firstPhaseRestarts /*since we cannot continue more runs in the second phase than we had runs in the first phase*/) {
//            System.out.println("returning 0: totalEvaluations<firstPhaseEvaluations*firstPhaseRestarts");
            return p.worstValue;
        }
        if (doLubyInFirstPhase && (totalTime < lubySum[firstPhaseRestarts-1]*firstPhaseTime)) {
            // return worst value if the unitTime * unitsNeeded exceeds totalTime
            return p.worstValue;
        }
        
        Solution[] results = new Solution[overallExperimentRepetitions];
        for (int i=0; i<overallExperimentRepetitions; i++) {
            if (debugPrint) System.out.println(">repeatExperiments i="+i);
            results[i] = doPhasedExperiments(p, totalTime, 
                    firstPhaseRestarts, firstPhaseTime, 
                    secondPhaseRestarts, secondPhaseTime, 
                    problemType, doLubyInFirstPhase);
            
            if (showIndividualResults) {
                if (secondPhaseRestarts==0) { 
                    // means: no second inner phase before the last phase
                    System.out.println(firstPhaseRestarts+","+firstPhaseTime+","+results[i].fitness);
                } else {
                    System.out.println(firstPhaseRestarts+","+firstPhaseTime+","+secondPhaseRestarts+","+secondPhaseTime+","+results[i].fitness);
                }
            }
        }
        
        // average fitness
        double sum = 0;
        for (int i=0; i<results.length; i++) {
            double currentFitness = results[i].fitness;
            sum += currentFitness;
        }        
        double avg = (sum/(double)results.length);
        if (debugPrint) {
            System.out.print(">results: ");
            for (int i=0; i<results.length; i++) {
                System.out.print(results[i].fitness+" ");
                if (i==results.length-1) System.out.println();
            }
            System.out.println(" avg=" + avg); 
        }
        
        return avg;
    }
    
    // following page 9 of "Optimal Speedup of Las Vegas Algorithm"
    public static int[] computeLubySequence() {
        int targetLength = 10; // 2^20=1048576
        
        boolean debugPrint = !true;
        
        int arrayLength = (int)Math.pow(2,targetLength)-1;
        int[] t = new int[arrayLength];
        
        for (int k=1; k<=targetLength; k++) {
            
            if (debugPrint) System.out.println(Arrays.toString(t));
            
            for (int i=1; i<=arrayLength; i++) {
                if (i == ((int)Math.pow(2, k)-1)) {
                    t[i-1] = (int)Math.pow(2, k-1);
                } else {
                    if( (((int)Math.pow(2, k-1)) <= i) &&
                            (i< ((int)Math.pow(2, k)-1))      ) {
                        t[i-1] = t[ i - (int)(Math.pow(2, k-1)) + 1 -1];
                    }
                }
            }
        }
        if (debugPrint) {
            System.out.println(Arrays.toString(t));
            System.out.println(Arrays.toString(computeRunningSum(t)));
        }
        return t;
    }
    
    public static int[] computeRunningSum(int[] a) {
        int[] result = new int[a.length];

        result[0] = a[0];
        
        for (int i=1; i<a.length; i++) {
            result[i] = result[i-1] + a[i];
        }
        
        return result;
    }
    
    public static int[] lubySequence = computeLubySequence();
//            new int[]{
//        1,1,2,1,1,2,4, 1,1,2,1,1,2,4, 8,
//        1,1,2,1,1,2,4, 1,1,2,1,1,2,4, 8,16, 
//        1,1,2,1,1,2,4, 1,1,2,1,1,2,4, 8,
//        1,1,2,1,1,2,4, 1,1,2,1,1,2,4, 8,16, 32
////        1,1,2,1,1,2,4, 1,1,2,1,1,2,4, 8,
////        1,1,2,1,1,2,4, 1,1,2,1,1,2,4, 8,16, 
////        1,1,2,1,1,2,4, 1,1,2,1,1,2,4, 8,
////        1,1,2,1,1,2,4, 1,1,2,1,1,2,4, 8,16, 32, 64
//};
    public static int[] lubySum = computeRunningSum(lubySequence);
//            new int[]{
//        1,2,4,5,6,8,12,                 13,14,16,17,18,20,24,           32,
//        33,34,36,37,38,40,44,           45,46,48,49,50,52,56,           64,80,
//        81,82,84,85,86,88,92,           93,94,96,97,98,100,104,         112,
//        113,114,116,117,118,120,124,    125,126,128,129,130,132,136,    144,160, 192
//    };
    
    // based on http://stackoverflow.com/questions/18895915/how-to-sort-an-array-of-objects-in-java
    public static void sortSolutionsBestToWorst(Solution[] results, Problem p) {
        Arrays.sort(results, new Comparator<Solution>() {
        @Override
        public int compare(Solution o1, Solution o2) {
            int result = 0;
            if (p.isMaximisation) { // important distinction follows, so that we are independent of the minimisation/maximisation
                result = Double.compare(o2.fitness, o1.fitness);
            } else {
                result = Double.compare(o1.fitness, o2.fitness);
            }
            return result;
        }
        });
    }
    
    public static Solution doPhasedExperiments(Problem p, 
            double totalTime, 
            int firstPhaseRestarts, double firstPhaseTime, 
            int secondPhaseRestarts, double secondPhaseTime, 
            int problemType, boolean doLuby){
        
        boolean debugShowPhases = false;
        boolean debugPrint = false;
        Solution[] results = new Solution[firstPhaseRestarts];
        
        double lubyTimeUnit = firstPhaseTime;
        // quick check if Luby is actually doable 
        if (doLuby) {
            if (totalTime > lubyTimeUnit * lubySum[lubySum.length-1]) {
                System.out.println("EXIT: doPhasedExperiments, Luby sequence not long enough");
                System.exit(0);
            }
        }
        
        for (int i=0; i<firstPhaseRestarts; i++) {
            // overwrite time based on Luby sequence position
            if (doLuby) {
                firstPhaseTime = lubySequence[i]*lubyTimeUnit;
            }
            if (debugPrint) System.out.println("doPhasedExperiments i="+i+" firstPhaseTime="+firstPhaseTime);
            
            Algorithm a = null;
            switch (problemType) {
                case 1: 
                    a = new RLS(p,firstPhaseTime, null); 
                    break;
                case 2:
                    a = new LKH(p,firstPhaseTime, (int)(Math.random()*Integer.MAX_VALUE) ); 
                    break;
                case 3:
                    a = new NUMVC(p,firstPhaseTime, (int)(Math.random()*Integer.MAX_VALUE) ); 
                    break;
                case 4:
                    a = new CCLS2015(p,firstPhaseTime, (int)(Math.random()*Integer.MAX_VALUE) ); 
                    break;
                case 5:
                    a = new ACOTSPJavaTTP(p,firstPhaseTime, (int)(Math.random()*Integer.MAX_VALUE) ); 
                    break;
                default:
                    System.out.println("EXIT: doPhasedExperiments, incorrect algorithm");
                    System.exit(0);
            }
            results[i]=a.execute();
        }
        
        // average fitness and determine min/max
//        double sum = 0;
//        int maxIndex = 0;
//        int minIndex = 0;
//        double maxFitness = results[0].fitness;//Double.NEGATIVE_INFINITY;
//        double minFitness = results[0].fitness;//Double.POSITIVE_INFINITY;
//        for (int i=0; i<results.length; i++) {
//            double currentFitness = results[i].fitness;
//            sum += currentFitness;
//            if (currentFitness > maxFitness) {
//                maxIndex = i;
//                maxFitness = currentFitness;
//            }
//            if (currentFitness < minFitness) {
//                minIndex = i;
//                minFitness = currentFitness;
//            }
//        }
        if (debugShowPhases) {
            System.out.print("phase1 RESULTS: ");
            for (int i=0; i<results.length; i++) {
                System.out.print(results[i].fitness+" ");
                if (i==results.length-1) System.out.println();
            }
        }
//        if (debugShowPhases) System.out.println("phase1 max=" + maxFitness + "  min=" + minFitness + " avg=" + (sum/(double)results.length) +
//                 ", Luby:"+doLuby+","+firstPhaseRestarts+","+lubySum[firstPhaseRestarts-1]); // results[bestIndex]
        
        sortSolutionsBestToWorst(results,p);
        
        if (debugShowPhases) {
            System.out.print("phase1 RESULTS sorted: ");
            for (int i=0; i<results.length; i++) {
                System.out.print(results[i].fitness+" ");
                if (i==results.length-1) System.out.println();
            }
        }
        
        
        
        
        /* make the second phase another restart from k, but this time is it possible to have l>1 (IJCAI2016:l=1)*/
        if (secondPhaseRestarts>0) {
            Solution[] temps = new Solution[secondPhaseRestarts];
            for (int i=0; i<secondPhaseRestarts; i++) {
                Solution result = results[i];
                if (debugShowPhases) System.out.println("phase2 (secondPhaseTime="+secondPhaseTime+") starting with fitness=" + result.fitness + " seed="+result.seed + " timeNeeded="+result.timeNeeded);
                Algorithm a = null;
                switch (problemType) {
                    case 1: 
                        a = new RLS(p,secondPhaseTime, null); 
                        break;
                    case 2:
                        a = new LKH(p,result.timeNeeded+secondPhaseTime, result.seed); 
                        break;
                    case 3:
                        a = new NUMVC(p,result.timeNeeded+secondPhaseTime, result.seed); 
                        break;
                    case 4:
                        a = new CCLS2015(p,result.timeNeeded+secondPhaseTime, result.seed); 
                        break;
                    case 5:
                        a = new ACOTSPJavaTTP(p,result.timeNeeded+secondPhaseTime, result.seed); 
                        break;
                    default:
                        System.out.println("EXIT: doPhasedExperiments, incorrect algorithm (second phase)");
                        System.exit(0);
                }
            temps[i]=a.execute();
            }
            
            // make visible to the next phase, and sort those
            results = temps;
            sortSolutionsBestToWorst(results,p);
            
            if (debugShowPhases) {
                System.out.print("phase2 RESULTS sorted: ");
                for (int i=0; i<results.length; i++) {
                    System.out.print(results[i].fitness+" ");
                    if (i==results.length-1) System.out.println();
                }
            }
        }
        
        
        
        
        
        /* last phase: start the algorithm with the seed solution and with the remaining budget (evaluations or time in seconds) */
        double lastPhaseTime = totalTime - (firstPhaseTime * firstPhaseRestarts) - (secondPhaseTime * secondPhaseRestarts);
        // overwrite time based on Luby sequence position
        if (doLuby) {
            lastPhaseTime = totalTime - (lubySum[firstPhaseRestarts-1]*lubyTimeUnit); // LUBY is correctly supported only when there are a *total* of 2 phases (NO WARRANTIES for 3 TOTAL PHASES)
        }
        // select best for the final phase...
//        Solution result = (p.isMaximisation?results[maxIndex]:results[minIndex]); // default, in case all evaluations are used up in the first phase (then the next call will not result in any Solution instance)
        Solution result = results[0]; // was sorted with sortSolutionsBestToWorst
        
        if (debugShowPhases) System.out.println("phaseX (lastPhaseTime="+lastPhaseTime+") starting with fitness=" + result.fitness + " seed="+result.seed + " timeNeeded="+result.timeNeeded);
        
        if (lastPhaseTime > 0) {
            Algorithm a = null; // reseting algorithm configuration
            switch (problemType) {
                case 1: 
                    a = new RLS(p,lastPhaseTime, result);
                    break;
                case 2:
                    a = new LKH(p, result.timeNeeded/*so that it can catch up*/+lastPhaseTime, result.seed);  
                    break;
                case 3:
                    a = new NUMVC(p, result.timeNeeded/*so that it can catch up*/+lastPhaseTime, result.seed); 
                    break;
                case 4:
                    a = new CCLS2015(p, result.timeNeeded/*so that it can catch up*/+lastPhaseTime, result.seed); 
                    break;
                case 5:
                    a = new ACOTSPJavaTTP(p, result.timeNeeded/*so that it can catch up*/+lastPhaseTime, result.seed); 
                    break;
                default:
                    System.out.println("EXIT: doPhasedExperiments, incorrect algorithm (last phase)");
                    System.exit(1);
            }
            result = a.execute();
        }
        if (debugShowPhases) System.out.println("phaseX best=" + result.fitness + " seed="+result.seed + " timeNeeded="+result.timeNeeded);
        return result;
    }
}
