package restarts.algorithms;

import restarts.Algorithm;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import restarts.Problem;
import restarts.Solution;
import restarts.problems.TTP;
import restarts.utils.DeepCopy;

public class ACOTSPJavaTTP extends Algorithm implements Serializable {

    Solution finalSolution = null;
    
    public ACOTSPJavaTTP(Problem p, double totalTime, int seed) {
        super(p, totalTime, seed);
    }
    
    public Solution execute() {
        boolean debugPrint = false;
        
        TTP ttpProblem = (TTP)p;
//        if (debugPrint) System.out.println(tspProblem.ttpInstance);
        
        double result = Double.NEGATIVE_INFINITY;
        
        // run Linkern with seed and time limit on an instance
        result = mmasTour(ttpProblem);
        
        this.finalSolution = new Solution(1); // dummy used to pass the fitness around *AND* also the seed that resulted in that fitness
        this.finalSolution.seed = (Integer)this.seed;
        this.finalSolution.fitness = result;
        
        if (debugPrint) System.out.println("mmasttp.execute: fitness="+this.finalSolution.fitness+" totalTime="+this.totalTime+" seed="+this.seed);
        
        this.finalSolution.timeNeeded = totalTime; // no += since we start from scratch here based on the seed
        return this.finalSolution;
    }
    
    
    public static void main (String[] args) {
        TTP p = null;
        p = new TTP("a280_n279_bounded-strongly-corr_01.ttp");
        p = new TTP("dsj1000_n9990_bounded-strongly-corr_01.ttp");
        p = new TTP("ts225_n2240_uncorr_07.ttp");
        double maxTime = 20;
        int seed = 321;
        ACOTSPJavaTTP l = new ACOTSPJavaTTP(p, maxTime, seed);
        Solution resultSolution = l.execute();
//        double result = resultSolution.fitness;
        System.out.println(p.filename+" "+resultSolution.fitness+" (seed="+resultSolution.seed+")");//values[0]+")");
    }
    
    // helper function to execute Linkern, taken from TTP
    public double mmasTour(TTP instance) {
        boolean debugPrint = false;
        
        double result = Double.NEGATIVE_INFINITY;
        String filename = instance.subfolder+"/"+instance.filename;
        if (debugPrint) System.out.println("ACOTSPJava-TTP: "+instance.filename);
    
        try { // SETUP from ANTS 2016 paper
            List<String> command = new ArrayList<String>();
            // java -cp ACOTSPJava-TTP.jar de.adrianwilke.acotspjava.AcoTsp --rho 0.5 --alpha 1 --beta 2 --ants 25 --tours 100 --tries 1 --elitistants 100 --rasranks 6 --localsearch 3 --time 600 -seed 321 -i instances/vm1748_n17470_uncorr_02.ttp
            command.add("java");
            command.add("-cp");
            command.add("ACOTSPJava-TTP.jar");
            command.add("de.adrianwilke.acotspjava.AcoTsp");
            command.add("--rho");
            command.add("0.5");
            command.add("--alpha");
            command.add("1");
            command.add("--beta");
            command.add("2");
            command.add("--ants");
            command.add("25");
            command.add("--tours");
            command.add("100");
            command.add("--tries");
            command.add("1");
            command.add("--elitistants");
            command.add("100");
            command.add("--rasranks");
            command.add("6");
            command.add("--localsearch");
            command.add("4");                   // with boosting enabled, this correspondes to MMASls4boost from ANTS 2016
//            command.add("--boosting");        // time consuming: on dsj1000_n9990_bounded-strongly-corr_01.ttp this increases the runtime from about 8 seconds for the first output to about 50 seconds, on ts225_n2240_uncorr_07.ttp: from 3s to 8s
            command.add("--time");
            command.add(this.totalTime+"");
            command.add("-seed");
            command.add(this.seed+"");
            command.add("-i");
            command.add(filename);
            if (debugPrint || !true) printListOfStrings(command);

            ProcessBuilder builder = new ProcessBuilder(command);
            builder.redirectErrorStream(true);
            final Process process = builder.start();
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while ((line = br.readLine()) != null) {
                if (debugPrint) System.out.println("<ACOTSPJavaTTP> "+line);

                if (line.startsWith(instance.filename)) {
                    // extract fitness
                    int lastSpace = line.lastIndexOf(" ");
                    int secondButLastSpace = lastSpace-1;
                    // walk backwards
                    while (line.charAt(secondButLastSpace)!=' ') {
                        secondButLastSpace--;
                    }
                    String resultString = line.substring(secondButLastSpace,lastSpace);
                    result = Double.parseDouble(resultString);

                    if (debugPrint) System.out.println("<ACOTSPJavaTTP RESULT> "+result);
                }
            }

            if (debugPrint) System.out.println("Program terminated?");    
            int rc = process.waitFor();
            if (debugPrint) System.out.println("Program terminated!");
            
            } catch (Exception ex) {
            	ex.printStackTrace();
            }
        return result;
    }
    
}
