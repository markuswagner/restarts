package restarts.algorithms;

import restarts.Algorithm;
import java.io.Serializable;
import java.util.Arrays;
import restarts.Problem;
import restarts.Solution;
import restarts.utils.DeepCopy;

public class RLS extends Algorithm implements Serializable {
    
//    double totalTime = 0;
//    Problem p = null;
//    Solution seed = null;
    
    double currentEvaluations = 0;
    
    Solution initialSolution = null;
    double initialFitness = 0;
    Solution bestSoFar = null;
    double bestSoFarFitness = 0;
    
    boolean seeded = false;
    
    public RLS(Problem p, double totalTime, Solution seed) {
        super(p, totalTime, seed);
//        this.p = p;
//        this.maxEvaluations = maxEvaluations;
//        this.seed = seed;
        
        if (seed == null) {
            // no seeding --> default behaviour
            this.initialSolution = p.initialSolution();
            this.initialFitness = p.evaluate(this.initialSolution);
            this.currentEvaluations++;
        } else {
            this.initialSolution = (Solution)DeepCopy.copy(seed);
            this.initialFitness = seed.fitness;
            if (this.initialFitness == Double.NEGATIVE_INFINITY) {
                System.out.println("EXIT: initialised with a seed with this.initialFitness == Double.NEGATIVE_INFINITY");
                System.exit(0);
            }
            this.seeded = true;
            // seeding does not cost an evaluation
        }
        
        this.bestSoFar = (Solution)DeepCopy.copy(initialSolution);
        this.bestSoFarFitness = this.initialFitness;
        
    }
    
    public Solution execute() {
//        if (this.seeded) System.out.println("seeded with fitness="+initialSolution.fitness + " maxEvaluations=" + maxEvaluations);
        
        boolean debugPrint = !true;
        for (int i=(int)currentEvaluations; i<totalTime; i++) {
            if (bestSoFarFitness==p.bestValue) break;
            
            if (debugPrint) System.out.println(i+" "+bestSoFarFitness+" target="+p.bestValue);
            
            // copy
            Solution newSolution = (Solution) DeepCopy.copy(bestSoFar);
//            if (debugPrint) System.out.println("before mutation "+bestSoFarFitness + " " +Arrays.toString(bestSoFar.values));
            
            // mutate
            int position = (int)(Math.random()*initialSolution.values.length);
            double value = newSolution.values[position];
            newSolution.values[position] = (value==1)?0:1;
            
            double newFitness = p.evaluate(newSolution);
            if (debugPrint) System.out.println("mutated position "+position+" from "+value+" to "+newSolution.values[position]);
            
            if (newFitness > bestSoFarFitness) {
                this.bestSoFar = newSolution;
                this.bestSoFarFitness = newFitness;
            }
        }
//        System.out.println(this.bestSoFarFitness);
        this.bestSoFar.timeNeeded += totalTime;
        return this.bestSoFar;
    }
}
