package restarts.algorithms;

import restarts.Algorithm;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import restarts.problems.MAXSAT;
import restarts.Problem;
import restarts.Solution;
import restarts.utils.DeepCopy;

public class CCLS2015Tuning  implements Serializable {
    
//    Solution finalSolution = null;
//    
//    public CCLS2015Tuning(Problem p, double totalTime, int seed) {
//        super(p, totalTime, seed);
//    }
    
//    public Solution execute() {
//        boolean debugPrint = !true;
//        
//        MAXSAT maxsatProblem = (MAXSAT)p;
////        if (debugPrint) System.out.println(vcProblem.vcInstance);
//        
//        double result = Double.POSITIVE_INFINITY;
//        
//        // run Linkern with seed and time limit on an instance
//        result = maxsat(maxsatProblem);
//        
//        this.finalSolution = new Solution(1); // dummy used to pass the fitness around *AND* also the seed that resulted in that fitness
////        this.finalSolution.values[0] = -1;
//        
//        //this.finalSolution.values[0] = (Integer)this.seed;
//        this.finalSolution.seed = (Integer)this.seed;
//        this.finalSolution.fitness = result;
//        
//        if (debugPrint) System.out.println("CCLS2015.execute: fitness="+this.finalSolution.fitness+" totalTime="+this.totalTime+" seed="+this.seed);
//        
//        this.finalSolution.timeNeeded = totalTime; // no += since we start from scratch here based on the seed
//        return this.finalSolution;
//    }
    
    public static int bestSeed = 0;
    public static double bestTime = Double.MAX_VALUE;
    public static double bestValue = Double.MAX_VALUE;
    
    public static int firstSeed = 0;
    public static double firstTime = Double.MAX_VALUE;
    public static double firstValue = Double.MAX_VALUE;
    
    public static void main (String[] args) {
        System.out.println("CCLS2015tuning");
        
        MAXSAT p = new MAXSAT(args[0]);//"UR-s3v80c600-3.cnf"
//        p = new MAXSAT("UC-scpcyc11_maxsat.cnf");
        double maxTimePerRun = 300; // 300, in seconds
        double totalTimeForTuning = 24*3600*1000; // in ms
        
        long startTime = System.currentTimeMillis();
        
        boolean first = true;
        
        int counter = 0;
        
        while( ((System.currentTimeMillis()-startTime)<totalTimeForTuning) 
                && counter++<200 ) {
        
            int seed = (int)(Math.random()*Integer.MAX_VALUE);
//            CCLS2015Tuning l = new CCLS2015Tuning(p, maxTimePerRun, seed);
//            Solution resultSolution = l.execute();
            double timeHere = maxsat(p, maxTimePerRun, seed);
            
//            if (timeHere<bestTime) {
//                bestTime = timeHere;
//                bestSeed = seed;
//            }
//            System.out.println("end of iteration");
            
            if (first) {
                firstSeed = bestSeed;
                firstValue = bestValue;
                firstTime = bestTime;
                first = false;
            }
            
            if (bestTime < 20) break;
        }
//        double result = resultSolution.fitness;
        System.out.println(p.maxsatInstance+","+firstValue+","+firstTime+","+firstSeed);
        System.out.println(p.maxsatInstance+","+bestValue+","+bestTime+","+bestSeed);
    }
    
    public static void printListOfStrings(List<String> list) {
        String result = "";
        for (String s:list)
            result+=s+" ";
        System.out.println(result);
    }
    
    // helper function to execute NUMVC, copy of Linkern
    public static double maxsat(MAXSAT instance, double totalTime, int seed) {
        boolean debugPrint = !true;
        
        double result = Double.POSITIVE_INFINITY;
        String maxsatfilename = instance.subfolder+"/"+instance.maxsatInstance;
        if (debugPrint) System.out.println("CCLS2015: "+maxsatfilename);
    
        long startTime = System.currentTimeMillis();
        double bestValueSoFar = Double.MAX_VALUE;
        double bestValueSoFarTime = 0;
        
        try {
            List<String> command = new ArrayList<String>();
            command.add("./CCLS2015");
            command.add(maxsatfilename);
//            command.add(instance.bestValue+"");    // for MVC, we assume this to be 0, needs to be removed for fastvc
            command.add(seed+"");          // +"" used to convert the number into a string
            command.add(totalTime+"");       // +"" used to convert the number into a string
            
            if (true) printListOfStrings(command);

            ProcessBuilder builder = new ProcessBuilder(command);
            builder.redirectErrorStream(true);
            final Process process = builder.start();
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while ( 
                     ((System.currentTimeMillis()-startTime)<totalTime*1000) // speedup: stop when time exceeded
//                    && (System.currentTimeMillis()-startTime<bestTime*1.5)
                    ) {
//                System.out.println(line);
                
//                if ((System.currentTimeMillis()-startTime)>(bestTime*1.5)) break;
                
                if ((line = br.readLine()) == null) break;
                

                if (line.contains("o ")) {
                    // extract fitness
                    int colonPosition = line.lastIndexOf(" ");
                    String resultString = line.substring(colonPosition+1);
                    result = Double.parseDouble(resultString);
                    if (debugPrint) System.out.println("<CCLS2015 RESULT> "+result);
                    
                    long timeUntilNow = System.currentTimeMillis()-startTime;

                    
                    
//                    if (result < bestValueSoFar) {
//                        bestValueSoFar = result;
//                        bestValueSoFarTime = timeUntilNow;
//                    }

                    
                    double timeToBreak = bestTime+30*1000;
                    
                    if (result <= bestValue) {
                        
                        if ((result == bestValue) && (timeUntilNow < bestTime)) {
                            bestValue = result;
                            bestTime = timeUntilNow;
                            bestSeed = seed;
                            System.out.println("faster found: bestValue="+bestValue+" bestTime="+bestTime+" bestSeed="+bestSeed);
                        } else {
                            // if result is worse and more than 1.5 times the bestTime is used (or: +30 secs): skip
    //                        System.out.println(timeUntilNow+">"+timeToBreak+" ?");
                            if (timeUntilNow > timeToBreak) {
                                System.out.println(" time1 "+timeToBreak+" exceeded without being better, seed="+seed);
                                break;
                            }
                        }
                        
                        if (result < bestValue) {
                            bestValue = result;
                            bestTime = timeUntilNow;
                            bestSeed = seed;
                            System.out.println("better found: bestValue="+bestValue+" bestTime="+bestTime+" bestSeed="+bestSeed);
                        }
                        
                        
                    } else {
                        // if result is worse and more than 1.5 times the bestTime is used (or: +30 secs): skip
//                        System.out.println(timeUntilNow+">"+timeToBreak+" ?");
                        if (timeUntilNow > timeToBreak) {
                            System.out.println(" time2 "+timeToBreak+" exceeded without being better, seed="+seed);
                            break;
                        }
                    }
                    

                }
            }

            
            if (debugPrint) System.out.println("Program terminated?");    
            process.destroyForcibly().waitFor();
//            int rc = process.waitFor();
            if (debugPrint) System.out.println("Program terminated!");
            
            } catch (Exception ex) {
            	ex.printStackTrace();
            }
        return bestValueSoFarTime;
    }
    
    
    
}
