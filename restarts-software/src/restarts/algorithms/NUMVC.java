package restarts.algorithms;

import restarts.Algorithm;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import restarts.problems.MVC;
import restarts.Problem;
import restarts.Solution;
import restarts.utils.DeepCopy;

public class NUMVC extends Algorithm implements Serializable {
    
    Solution finalSolution = null;
    
    public NUMVC(Problem p, double totalTime, int seed) {
        super(p, totalTime, seed);
    }
    
    public Solution execute() {
        boolean debugPrint = !true;
        
        MVC vcProblem = (MVC)p;
//        if (debugPrint) System.out.println(vcProblem.vcInstance);
        
        double result = Double.POSITIVE_INFINITY;
        
        // run Linkern with seed and time limit on an instance
        result = vc(vcProblem);
        
        this.finalSolution = new Solution(1); // dummy used to pass the fitness around *AND* also the seed that resulted in that fitness
//        this.finalSolution.values[0] = -1;
        
//        this.finalSolution.values[0] = (Integer)this.seed;
        this.finalSolution.seed = (Integer)this.seed;
        this.finalSolution.fitness = result;
        
        if (debugPrint) System.out.println("NUMVC.execute: fitness="+this.finalSolution.fitness+" totalTime="+this.totalTime+" seed="+this.seed);
        
        this.finalSolution.timeNeeded = totalTime; // no += since we start from scratch here based on the seed
        return this.finalSolution;
    }
    
    
    public static void main (String[] args) {
        MVC p = new MVC("frb100-40.mis");
        p = new MVC("large-rec-amazon.mtx.47606.mis");
        double maxTime = 1;
        int seed = 14;
        NUMVC l = new NUMVC(p, maxTime, seed);
        Solution resultSolution = l.execute();
//        double result = resultSolution.fitness;
        System.out.println(p.vcInstance+" "+resultSolution.fitness+" (seed="+resultSolution.seed+")");//values[0]+")");
    }
    
    // helper function to execute NUMVC, copy of Linkern
    public double vc(MVC instance) {
        boolean debugPrint = !true;
        
        double result = Double.POSITIVE_INFINITY;
        String vcfilename = instance.subfolder+"/"+instance.vcInstance;
        if (debugPrint) System.out.println("NUMVC: "+vcfilename);
    
        try {
            List<String> command = new ArrayList<String>();
            command.add("./numvc");
            command.add(vcfilename);
//            command.add(instance.bestValue+"");    // for MVC, we assume this to be 0, needs to be removed for fastvc
            command.add(this.seed+"");          // +"" used to convert the number into a string
            command.add(this.totalTime+"");       // +"" used to convert the number into a string
            
            if (debugPrint || !true) printListOfStrings(command);

            ProcessBuilder builder = new ProcessBuilder(command);
            builder.redirectErrorStream(true);
            final Process process = builder.start();
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while ((line = br.readLine()) != null) {
                if (debugPrint) System.out.println("<NUMVC> "+line);

                if (line.contains("Best found vertex cover size")) {
                    // extract fitness
                    int colonPosition = line.lastIndexOf("=");
                    String timeString = line.substring(colonPosition+2);

                    result = Double.parseDouble(timeString);

                    if (debugPrint) System.out.println("<NUMVC RESULT> "+result);
                }
            }

            if (debugPrint) System.out.println("Program terminated?");    
            int rc = process.waitFor();
            if (debugPrint) System.out.println("Program terminated!");
            
            } catch (Exception ex) {
            	ex.printStackTrace();
            }
        return result;
    }
    
    
    
}
