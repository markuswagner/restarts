package restarts.algorithms;

import restarts.Algorithm;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import restarts.Problem;
import restarts.Solution;
import restarts.problems.TSP;
import restarts.utils.DeepCopy;

public class LKH extends Algorithm implements Serializable {
    
//    int seed = Integer.MIN_VALUE; // seeding number for Linkern, to have reproducible runs

    Solution finalSolution = null;
    
    public LKH(Problem p, double totalTime, int seed) {
        super(p, totalTime, seed);
    }
    
    public Solution execute() {
        boolean debugPrint = !true;
        
        TSP tspProblem = (TSP)p;
//        if (debugPrint) System.out.println(tspProblem.tspInstance);
        
        double result = Double.POSITIVE_INFINITY;
        
        // run Linkern with seed and time limit on an instance
        result = linkernTour(tspProblem);
        
        this.finalSolution = new Solution(1); // dummy used to pass the fitness around *AND* also the seed that resulted in that fitness
//        this.finalSolution.values[0] = -1;
        
//        this.finalSolution.values[0] = (Integer)this.seed;
        this.finalSolution.seed = (Integer)this.seed;
        this.finalSolution.fitness = result;
        
        if (debugPrint) System.out.println("Linkern.execute: fitness="+this.finalSolution.fitness+" totalTime="+this.totalTime+" seed="+this.seed);
        
        this.finalSolution.timeNeeded = totalTime; // no += since we start from scratch here based on the seed
        return this.finalSolution;
    }
    
    
    public static void main (String[] args) {
        TSP p = new TSP("d15112.tsp");
        double maxTime = 0.5;
        int seed = 10;
        LKH l = new LKH(p, maxTime, seed);
        Solution resultSolution = l.execute();
//        double result = resultSolution.fitness;
        System.out.println(p.tspInstance+" "+resultSolution.fitness+" (seed="+resultSolution.seed+")");//values[0]+")");
    }
    
    // helper function to execute Linkern, taken from TTP
    public double linkernTour(TSP instance) {
        boolean debugPrint = !true;
        
        double result = Double.POSITIVE_INFINITY;
        String tspfilename = instance.subfolder+"/"+instance.tspInstance;
        if (debugPrint) System.out.println("LINKERN: "+tspfilename);
    
        try {
            List<String> command = new ArrayList<String>();
            if(System.getProperty("os.name").contains("Windows")){//WINDOWS OS
                    command.add("linkern-windows.exe");
            }else if(System.getProperty("os.name").contains("Mac")){// MAC OS
                    command.add("./linkern-macos");
            }else if(System.getProperty("os.name").contains("nix") || System.getProperty("os.name").contains("nux")){// Linux
                    command.add("./linkern-linux");
            }else {
                System.out.println("@linkernTour: incorrect OS?");
                System.exit(0);
            }
            command.add("-s");
            command.add(this.seed+"");          // +"" used to convert the number into a string
            command.add("-t");
            command.add(this.totalTime+"");       // +"" used to convert the number into a string
            command.add(tspfilename);
            if (debugPrint || !true) printListOfStrings(command);

            ProcessBuilder builder = new ProcessBuilder(command);
            builder.redirectErrorStream(true);
            final Process process = builder.start();
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while ((line = br.readLine()) != null) {
                if (debugPrint) System.out.println("<LINKERN> "+line);

                if (line.contains("Final Cycle")) {
                    // extract fitness
                    int colonPosition = line.lastIndexOf(":");
                    String timeString = line.substring(colonPosition+2);

                    result = Double.parseDouble(timeString);

                    if (debugPrint) System.out.println("<LINKERN RESULT> "+result);
                }
            }

            if (debugPrint) System.out.println("Program terminated?");    
            int rc = process.waitFor();
            if (debugPrint) System.out.println("Program terminated!");
            
            } catch (Exception ex) {
            	ex.printStackTrace();
            }
        return result;
    }
    
}
