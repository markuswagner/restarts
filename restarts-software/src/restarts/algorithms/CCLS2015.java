package restarts.algorithms;

import restarts.Algorithm;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import restarts.problems.MAXSAT;
import restarts.Problem;
import restarts.Solution;
import restarts.utils.DeepCopy;

public class CCLS2015 extends Algorithm implements Serializable {
    
    Solution finalSolution = null;
    
    public CCLS2015(Problem p, double totalTime, int seed) {
        super(p, totalTime, seed);
    }
    
    public Solution execute() {
        boolean debugPrint = !true;
        
        MAXSAT maxsatProblem = (MAXSAT)p;
//        if (debugPrint) System.out.println(vcProblem.vcInstance);
        
        double result = Double.POSITIVE_INFINITY;
        
        // run Linkern with seed and time limit on an instance
        result = maxsat(maxsatProblem);
        
        this.finalSolution = new Solution(1); // dummy used to pass the fitness around *AND* also the seed that resulted in that fitness
//        this.finalSolution.values[0] = -1;
        
        //this.finalSolution.values[0] = (Integer)this.seed;
        this.finalSolution.seed = (Integer)this.seed;
        this.finalSolution.fitness = result;
        
        if (debugPrint) System.out.println("CCLS2015.execute: fitness="+this.finalSolution.fitness+" totalTime="+this.totalTime+" seed="+this.seed);
        
        this.finalSolution.timeNeeded = totalTime; // no += since we start from scratch here based on the seed
        return this.finalSolution;
    }
    
    
    public static void main (String[] args) {
        MAXSAT p = new MAXSAT("UR-s3v80c600-3.cnf");
//        p = new MAXSAT("UC-scpcyc11_maxsat.cnf");
        double maxTime = 10;
        int seed = 15;
        CCLS2015 l = new CCLS2015(p, maxTime, seed);
        Solution resultSolution = l.execute();
//        double result = resultSolution.fitness;
        System.out.println(p.maxsatInstance+" "+resultSolution.fitness+" (seed="+resultSolution.seed+")");//values[0]+")");
    }
    
    // helper function to execute NUMVC, copy of Linkern
    public double maxsat(MAXSAT instance) {
        boolean debugPrint = !true;
        
        double result = Double.POSITIVE_INFINITY;
        String maxsatfilename = instance.subfolder+"/"+instance.maxsatInstance;
        if (debugPrint) System.out.println("CCLS2015: "+maxsatfilename);
    
        try {
            List<String> command = new ArrayList<String>();
            command.add("./CCLS2015");
            command.add(maxsatfilename);
//            command.add(instance.bestValue+"");    // for MVC, we assume this to be 0, needs to be removed for fastvc
            command.add(this.seed+"");          // +"" used to convert the number into a string
            command.add(this.totalTime+"");       // +"" used to convert the number into a string
            
            if (debugPrint || !true) printListOfStrings(command);

            ProcessBuilder builder = new ProcessBuilder(command);
            builder.redirectErrorStream(true);
            final Process process = builder.start();
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while ((line = br.readLine()) != null) {
                if (debugPrint) System.out.println("<CCLS2015> "+line);

                if (line.contains("o ")) {
                    // extract fitness
                    int colonPosition = line.lastIndexOf(" ");
                    String resultString = line.substring(colonPosition+1);

                    result = Double.parseDouble(resultString);

                    if (debugPrint) System.out.println("<CCLS2015 RESULT> "+result);
                }
            }

            if (debugPrint) System.out.println("Program terminated?");    
            int rc = process.waitFor();
            if (debugPrint) System.out.println("Program terminated!");
            
            } catch (Exception ex) {
            	ex.printStackTrace();
            }
        return result;
    }
    
    
    
}
