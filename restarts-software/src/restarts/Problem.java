package restarts;

import java.io.Serializable;

public abstract class Problem implements Serializable {
    
//    int size = 0;
//    
//    OneMax(int size) {
//        this.size = size;
//    }
    
    public Problem(double bestValue, double worstValue, boolean isMaximisation) {
        this.bestValue = bestValue;
        this.worstValue = worstValue;
        this.isMaximisation = isMaximisation;
    }
    
    public double bestValue;
    public double worstValue;
    
    public boolean isMaximisation = true;
    
    public abstract double evaluate(Solution solution);
    
    public abstract Solution initialSolution();
}
