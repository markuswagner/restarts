package restarts;

import java.io.Serializable;

public class Solution implements Serializable {
    public double[] values = null; // used only by some problems
    public int seed = -1;
    public double fitness = Double.NEGATIVE_INFINITY;
    public double timeNeeded = 0;
    
    public Solution(int n) {
//        this.values = new double[n];
        this.seed = n;
    }
}
